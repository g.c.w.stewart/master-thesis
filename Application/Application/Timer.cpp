#include "stdafx.h"
#include <iostream>
#include <conio.h>
#include <time.h>

#include "Timer.h"

Timer::Timer() {
	resetted = true;
	running = false;
	beg = 0;
	end = 0;
}

void Timer::start() {
	if (!running) {
		if (resetted)
			beg = (unsigned long)clock();
		else
			beg -= end - (unsigned long)clock();
		running = true;
		resetted = false;
	}
}

void Timer::stop() {
	if (running) {
		end = (unsigned long)clock();
		running = false;
	}
}

void Timer::reset() {
	bool wasRunning = running;
	if (wasRunning)
		stop();
	resetted = true;
	beg = 0;
	end = 0;
	if (wasRunning)
		start();
}

bool Timer::isRunning() {
	return running;
}

double Timer::getTime() {
	if (running)
		return (double)(clock() - beg) / (double)CLOCKS_PER_SEC;
	return (double)(end - beg) / (double)CLOCKS_PER_SEC;
}

bool Timer::isOver(unsigned long seconds) {
	return seconds >= getTime();
}
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

__device__ int bitShiftLeftGPU(int toShift, int placesToShift) {
	int result = toShift;
	for (int i = 0; i < placesToShift; i++)
		result *= 3;
	return result;
}

__device__ int bitShiftRightGPU(int toShift, int placesToShift) {
	int result = toShift;
	for (int i = 0; i < placesToShift; i++)
		result /= 3;
	return result;
}

__device__ int valueAtIndexGPU(int number, int index) {
	int newNumber = bitShiftRightGPU(number, index);
	int value = newNumber % 3;
	return value;
}

__device__ int removeValueAtIndexGPU(int number, int index, int value) {
	return number - bitShiftLeftGPU(1, index) * value;
}
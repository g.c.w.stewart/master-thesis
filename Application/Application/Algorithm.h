#pragma once
#include <vector>

#include "Graph.h"
#include "Instruction.h"
#include "Global.h"

class Algorithm{
public:
	virtual std::pair<int, double> runAlgorithm(TreeDecomposition* td) = 0;
protected:
	virtual int sizeOfStepOrArray(int exponent) = 0;

	std::vector<Instruction*> traversal;
	std::vector<Vertex*> currentBag;
	std::vector<std::vector<Vertex*>> previousBags;
	std::vector<int> previousBagPath;
	std::vector<Instruction> instructions;

};
#pragma once
#include <vector>

/*
For Dominating Set:
0: White
1: Grey
2: Black
*/

class TernaryOperations {
public:
	static int bitShiftLeft(int toShift, int placesToShift); // bitshift left
	static int bitShiftRight(int toShift, int placesToShift); // bitshift right
	static int valueAtIndex(int number, int index); // gets value at index
	static int removeValueAtIndex(int number, int index, int value); // removes value at given index
};
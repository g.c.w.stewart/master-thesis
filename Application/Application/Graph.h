#pragma once

#include <vector>
#include "Instruction.h"
#include <limits>


typedef std::vector<std::vector<int>> TreeDecomp;

class Edge;

class Vertex {
public:
	int id;
	int Color = -1;
	std::vector<Edge*> Adjacent;
	Vertex();
	~Vertex();
	Vertex(int id);
};

class Edge {
public:
	Vertex* From;
	Vertex* To;
	Edge();
	Edge(Vertex* From, Vertex* To);
};

class OG {
public:
	OG();
	~OG();
	static OG* Parse(std::ifstream& inFile);
	void addEdge(int a, int b, OG* g);

	std::vector<Vertex*> Vertices;
	std::vector<Edge*> Edges;
};


class TreeDecomposition {
public:
	TreeDecomposition();
	TreeDecomposition(int nodeCount, int width);
	~TreeDecomposition();

	std::vector<Instruction*> Compute();
	static TreeDecomposition* Parse(std::ifstream& inFile, OG* g);
	void FindOptimalRoot();

	int width;
	int numberOfNodes;
	double EstimatedCost = std::numeric_limits<double>::max();
	std::vector<TDNode*> Nodes;
	std::vector<TDNode*> Leaves;
	OG* ParentGraph;
	TDNode* Root;
};
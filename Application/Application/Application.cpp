// Application.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "Global.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include "Output.h"

#include "Algorithm_CPU_MIS.h"
#include "Algorithm_CPU_DS.h"
#include "Algorithm_GPU_MIS.cuh"
#include "Algorithm_GPU_DS.cuh"

using namespace std;
static OG g;

std::pair<int, double> runAlgorithm(std::vector<Instruction*> traversal, TreeDecomposition* td) {

	std::pair<int, double> answerAndTime;
	if (!Global::useGPU) {
		if (Global::useMIS) {
			Algorithm_CPU_MIS alg(traversal);
			answerAndTime = alg.runAlgorithm(td);
		}
		else {
			Algorithm_CPU_DS alg(traversal);
			answerAndTime = alg.runAlgorithm(td);
		}
	}
	else {
		if (Global::useMIS) {
			Algorithm_GPU_MIS alg(traversal);
			answerAndTime = alg.runAlgorithm(td);
		}
		else {
			Algorithm_GPU_DS alg(traversal);
			answerAndTime = alg.runAlgorithm(td);
		}
	}
	return answerAndTime;
}


void singleRun() {
	for (int j = 89; j < Global::numberOfGraphs; j += 2) {
		std::vector<string> results;
		int bagCount;
		int nodeCount = 0;
		int edgeCount = 0;
		int width = 0;

		std::ostringstream instanceNumber;
		instanceNumber << std::setfill('0') << std::setw(3) << j;
		std::string number = instanceNumber.str();
		std::cout << number << "" << std::endl;
		std::string path = "C:\\Users\\glenn_000\\Desktop\\Thesis\\master-thesis\\exactLowTw\\public\\instance" + number + ".gr";
		std::ifstream inFile;
		inFile.open(path);

		std::cout << "Making Tree" << std::endl;

		Timer timer;

		double time2 = timer.getTime();

		OG* g;
		g = OG::Parse(inFile);
		static TreeDecomposition* td;
		td = TreeDecomposition::Parse(inFile, g);

		timer.start();

		td->FindOptimalRoot();

		timer.stop();
		time2 = timer.getTime();
		std::cout << "Graph parse time = " << time2 << std::endl;
		timer.reset();

		std::vector<Instruction*> traversal = td->Compute();

		inFile.close();
		std::cout << "Computing answer" << std::endl;
		for (int i = 0; i < Global::numberOfRuns; i++) {
			try {
				std::pair<int, double> answerAndTime = runAlgorithm(traversal, td);
				int answer = answerAndTime.first;
				double time = answerAndTime.second;

				std::cout << "Computing answer time = " << time << std::endl;
				std::cout << std::endl;
				string result = std::to_string(answer) + " ; " + std::to_string(time);
				results.push_back(result);
			}
			catch (const std::exception& e) {
				std::cout << e.what() << std::endl;
				string result = std::string(e.what()) + " ; " + std::to_string(-1);
				results.push_back(result);
				break;
			}
		}


		bagCount = td->numberOfNodes;
		width = td->width - 1;
		nodeCount = g->Vertices.size();
		edgeCount = g->Edges.size();
		delete td;
		delete g;
		Output::printToFile("C:\\Users\\glenn_000\\Desktop\\Thesis\\master-thesis\\results\\", 
			results, bagCount, nodeCount, edgeCount, width, j);
	}
}

int main()
{
	srand(13);
	singleRun();
	return 0;
}
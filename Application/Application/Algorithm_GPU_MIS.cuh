#pragma once
#include <vector>
#include <set>
#include <map>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include "Graph.h"
#include "Instruction.h"
#include "Global.h"
#include "Algorithm_GPU.h"

class Algorithm_GPU_MIS : public Algorithm_GPU {
public:
	Algorithm_GPU_MIS(std::vector<Instruction*> traversal);
private:
	int sizeThisStep;
	int sizePreviousStep;

	int sizeOfStepOrArray(int exponent);
	cudaError_t launchKernel(unsigned int newSize, Instruction::InstructionType instructionType, int bagSize, bool isLast);
	std::pair<int, int> getKernelParameters(unsigned int newSize);
};
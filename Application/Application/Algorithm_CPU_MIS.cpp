#include "stdafx.h"
#include "Algorithm_CPU_MIS.h"
#include <algorithm>  

Algorithm_CPU_MIS::Algorithm_CPU_MIS(std::vector<Instruction*> traversal) {
	this->traversal = traversal;
}

int Algorithm_CPU_MIS::sizeOfStepOrArray(int exponent) {
	return 1 << exponent;
}

int Algorithm_CPU_MIS::leafBag() {
	return 0;	
}

int Algorithm_CPU_MIS::introduceBag(int index, int nodeIndex, bool isLast) {
	int nodePositionMask = 1 << nodeIndex;
	int isInSet = (index & nodePositionMask) >> nodeIndex;

	if (isLast) {
		if (isInSet == 1) {
			int newIndex = index - nodePositionMask;
			return oldValues.back()[newIndex];
		}	
	}	
	else {
		if (isInSet == 1) {
			int leftside = index >> nodeIndex;
			leftside = leftside << nodeIndex;
			int rightside = index - leftside;
			leftside = leftside >> (nodeIndex + 1);
			leftside = leftside << nodeIndex;
			return oldValues.back()[leftside + rightside];
		}
		else {
			int leftside = index >> (nodeIndex + 1);
			leftside = leftside << nodeIndex;
			int rightside = index - (leftside << 1);
			return oldValues.back()[leftside + rightside];
		}
	}
	return oldValues.back()[index];
}	

int Algorithm_CPU_MIS::introduceEdgeBag(int index, int node1Index, int node2Index) {
	int positionMask1 = 1 << node1Index;
	int isInSet1 = (index & positionMask1) >> node1Index;

	int positionMask2 = 1 << node2Index;
	int isInSet2 = (index & positionMask2) >> node2Index;

	if (isInSet1 == 1) {
		if (isInSet2 == 1) {
			return Global::minValue;
		}
		else {
			return oldValues.back()[index];
		}
	}
	else {
		return oldValues.back()[index];
	}
}

int Algorithm_CPU_MIS::forgetBag(int index, int previousNodeIndex, int thisStepSize) {
	int leftMask = (thisStepSize - 1) & ((thisStepSize - 1) << previousNodeIndex);
	int rightMask = (thisStepSize - 1) - leftMask;

	int leftSide = (index & leftMask) << 1;
	int rightSide = (index & rightMask);
	int combined = leftSide + rightSide;

	int indexOption1 = combined; //Set without node
	int indexOption2 = combined + (1 << previousNodeIndex); //Set with node

	int option1 = this->oldValues.back()[indexOption1];
	int option2 = this->oldValues.back()[indexOption2] + 1;

	return std::max(option1, option2);
}

int Algorithm_CPU_MIS::joinBag(int index, int bagSize) {
	int valuePreviousBag = this->oldValues.back()[index];
	int valuePreviousBag2 = this->oldValues.end()[-2][index];

	if (valuePreviousBag < 0 || valuePreviousBag2 < 0)
		return Global::minValue;
	return valuePreviousBag + valuePreviousBag2;
}
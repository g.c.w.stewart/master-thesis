#pragma once

#include "Global.h"
//#include "Graph.h"
#include <map>
#include <cstddef>
#include <list>

class Vertex;
class TreeDecomposition;

class Instruction {
public:
	enum InstructionType { Leaf, Introduce, IntroduceEdge, Forget, Join, Empty };
	Instruction();
	Instruction(InstructionType bagType, std::vector<Vertex*> bag = {}, bool prevIsChild = true, int node1 = -1, int node2 = -1);
	InstructionType getInstructionType();
	int getNode1();
	int getNode2();
	bool prevIsChild;

	std::vector<Vertex*> bag;

private:
	InstructionType instructionType;
	int node1;
	int node2;
};

class TDNode {
public:
	TDNode();
	~TDNode();
	TreeDecomposition* ParentDecomposition;

	std::vector<TDNode*> Adjacent;
	std::vector<Vertex*> VertexByColor;
	std::vector<Vertex*> Bag;
	std::map<int, Vertex*> Subtree;
	std::vector<int> VertexIndex;

	std::vector<Vertex*> OptimizedBag;
	std::vector<Vertex*> OptimizedByColor;

	double CalculateCost();
	double CalculateCost(TDNode* parent);
	void FillSubtree(TDNode* parent);
	void SetBagsFinal();
	void PreJoinForget(TDNode* parent);
	void DeconstructJoins(TDNode* parent);
	void ForgetBeforeIntroduce(TDNode* parent);

	bool BagContains(Vertex* v);
	void IndexVertices(TDNode* parent);
	void ColorVertices();
	void ColorVertices(TDNode* parent);
	static int BitCount(int i);
	void Compute(std::vector<Instruction*>* traversal, TDNode* parent);
	void IntroduceIfNecessary(std::vector<Instruction*>* traversal, TDNode* child);

	void TDNode::ComputeIntroduce(TDNode* bag, TDNode* child, std::vector<Vertex*> vertices, std::vector<Instruction*>* traversal);
	void TDNode::ComputeForget(TDNode* bag, TDNode* child, std::vector<Vertex*> vertices, std::vector<Instruction*>* traversal);
	void TDNode::ComputeJoin(TDNode* bag, TDNode* left, TDNode* right, std::vector<Instruction*>* traversal);
	void TDNode::ComputeIntroduceEdge(Vertex* vertex, std::vector<Instruction*>* traversal);
};
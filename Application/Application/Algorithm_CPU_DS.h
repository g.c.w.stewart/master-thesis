#pragma once
#include <stdio.h>
#include <vector>
#include <set>
#include <map>

#include "Graph.h"
#include "Instruction.h"
#include "Global.h"
#include "Algorithm_CPU.h"

class Algorithm_CPU_DS : public Algorithm_CPU {
public:
	Algorithm_CPU_DS(std::vector<Instruction*> traversal);
private:
	int sizeOfStepOrArray(int exponent);
	int leafBag();
	int introduceBag(int index, int node, bool isLast = true);
	int introduceEdgeBag(int index, int node1, int node2);
	int forgetBag(int index, int previousNodeIndex, int thisStepSize);
	int joinBag(int index, int bagSize);
};
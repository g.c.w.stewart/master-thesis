#pragma once

#include "Algorithm_CPU.h"
#include "Global.h"
#include "Instruction.h"
#include "Graph.h"

class Algorithm_CPU_MIS : public Algorithm_CPU {
public:
	Algorithm_CPU_MIS(std::vector<Instruction*> traversal);
private:
	int sizeOfStepOrArray(int exponent);
	int leafBag();
	int introduceBag(int index, int nodeIndex, bool isLast = true);
	int introduceEdgeBag(int index, int node1, int node2);
	int forgetBag(int index, int node1, int node2);
	int joinBag(int index, int bagSize);
};
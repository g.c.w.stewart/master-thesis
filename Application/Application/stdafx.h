// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

#include "Global.h"
#include "Graph.h"
#include "Instruction.h"
#include "Algorithm.h"
#include "Algorithm_CPU.h"
#include "Algorithm_CPU_MIS.h"
#include "Algorithm_GPU.h"
#include "Algorithm_GPU_MIS.cuh"
#include "Output.h"
#include "TernaryOperations.h"
#include "TimeLogger.h"
#include "Timer.h"


// Reference additional headers your program requires here

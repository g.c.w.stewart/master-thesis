#pragma once
#include <iostream>
#include <string>

#include "Global.h"

class TimeLogger { // logs the time and answer of each run of the algorithm
public:
	static TimeLogger* Instance();
	void addTimeAndAnswer(int k, double time, int answer); // add a run time and answer to the array
	std::string getTimesListString(); // returns the times of each run
private:
	double* timeList = new double[Global::numberOfRuns];
	int* answerList = new int[Global::numberOfRuns];
	static TimeLogger* _instance;
	void flushLists();
};
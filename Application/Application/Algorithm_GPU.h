	#pragma once
#include <stdio.h>
#include <vector>
#include <set>
#include <map>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include "Graph.h"
#include "Instruction.h"
#include "Global.h"
#include "Algorithm.h"

class Algorithm_GPU : public Algorithm {
public:
	std::pair<int, double> runAlgorithm(TreeDecomposition* td);
protected:
	int sizeThisStep;
	int sizePreviousStep;

	int *dev_nodeIndex = 0;
	std::map<int, std::vector<int*>> notUsedMemory;

	// pointers to the allocated memory addresses containing the values
	std::vector<int*> valuePointers;
	std::vector<long> pointerSizes;
	unsigned int newPointerPosition = 0;

	std::vector<int*> indexLocationPointers;
	std::vector<int*> joinDataPerWidthPointers;

	virtual int sizeOfStepOrArray(int exponent) = 0;
	virtual std::pair<int, int> getKernelParameters(unsigned int newSize) = 0;
	virtual cudaError_t launchKernel(unsigned int newSize, Instruction::InstructionType instructionType, int bagSize, bool isLast = true) = 0;
	cudaError_t cudaInit();
	cudaError_t allocateMemory(int* memoryLocation, long arraySize);
	cudaError_t cudaFinish();
	cudaError_t cudaOperation(const int *nodeIndex, unsigned int newSize, Instruction::InstructionType type, bool prevIsChild, int bagSize, bool isLast = true);
	virtual void freeAllMemory();
	virtual void freeMemory(int* address);
};
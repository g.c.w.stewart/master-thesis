#pragma once

#include "stdafx.h"
#include "Instruction.h"
#include <algorithm>
#include <math.h> 
#include <queue>
#include <tuple>

Instruction::Instruction() {
	this->instructionType = Empty;
	this->node1 = -1;
	this->node2 = -1;
}

Instruction::Instruction(InstructionType instructionType, std::vector<Vertex*> bag, bool prevIsChild, int node1, int node2) {
	this->instructionType = instructionType;
	this->node1 = node1;
	this->node2 = node2;
	this->prevIsChild = prevIsChild;
	this->bag = bag;
}

Instruction::InstructionType Instruction::getInstructionType() {
	return this->instructionType;
}
	
int Instruction::getNode1() {
	return this->node1;
}

int Instruction::getNode2() {
	return this->node2;
}


TDNode::TDNode() {

}

TDNode::~TDNode() {
	Adjacent.clear();
	VertexByColor.clear();
	Bag.clear();
	Subtree.clear();
	VertexIndex.clear();
	OptimizedBag.clear();
	OptimizedByColor.clear();
}

double TDNode::CalculateCost() {
	FillSubtree(NULL);

	for (TDNode* n : ParentDecomposition->Nodes) {
		n->OptimizedBag = n->Bag;
		n->OptimizedByColor = n->VertexByColor;
	}

	return CalculateCost(NULL);
}

double TDNode::CalculateCost(TDNode* parent) {

	std::list<TDNode*> children;
	std::copy_if(Adjacent.begin(), Adjacent.end(), std::back_inserter(children),
		[parent](TDNode* i){return i != parent; });

	if (children.size() == 1){
		double temp = children.front()->CalculateCost(this) + pow(4.0, std::max(static_cast<int>(OptimizedBag.size()), static_cast<int>(children.front()->OptimizedBag.size())) - ParentDecomposition->width);
		return  temp;
	}

	if (children.size() == 0)
		return 0.0;

	// Strange (incorrect?) way of computing cost for a join
	double result = 0;
	for (TDNode* child : children)
		result += child->CalculateCost(this);

	//todo
	std::vector<int> q;
	for (TDNode* child : children)
		q.push_back(static_cast<int>(child->OptimizedBag.size()));
	std::sort(q.begin(), q.end());

	while (q.size() > 2) {
		int a = q.front();
		q.erase(q.begin());
		int b = q.front();
		q.erase(q.begin());
		q.push_back(std::min(std::max(a, b) + 1, static_cast<int>(OptimizedBag.size())));
		result += pow(5, std::max(a, b) + 1 - ParentDecomposition->width);
	}

	return result;
}

void TDNode::FillSubtree(TDNode* parent) 	{

	for (TDNode* n : Adjacent) {
		if (n != parent) {
			n->FillSubtree(this);
			for (Vertex* v : Bag) {
				for (Edge* e : v->Adjacent) {
					if (n->Subtree.count(e->To->id))
						Subtree[e->To->id] = e->To;
				}
			}
		}
	}

	for (Vertex* v : Bag)
		Subtree[v->id] = v;
}

void TDNode::SetBagsFinal() {
	Bag = OptimizedBag;
	VertexByColor = OptimizedByColor;
}

void TDNode::IndexVertices(TDNode* parent) {
	for (TDNode* n : Adjacent)
		if (n != parent)
			n->IndexVertices(this);

	std::sort(Bag.begin(), Bag.end(), [](Vertex* i, Vertex* j) {return (i->Color < j->Color); });

	VertexIndex = std::vector<int>(VertexByColor.size());

	for (unsigned int i = 0; i < Bag.size(); i++)
		VertexIndex[Bag[i]->Color] = i;
}

void TDNode::PreJoinForget(TDNode* parent) {
	std::list<TDNode*> children;
	std::copy_if(Adjacent.begin(), Adjacent.end(), std::back_inserter(children),
		[parent](TDNode* i){return i != parent; });

	if (children.size() == 1) {
		children.front()->PreJoinForget(this);
		return;
	}

	if (children.size() == 0)
		return;

	for (unsigned int i = 0; i < Adjacent.size(); i++) {
		if (Adjacent[i] == parent) continue;

		bool notContains = false;
		for (Vertex* v : Adjacent[i]->Bag) {
			bool containsV = this->BagContains(v);
			if (!containsV)
				notContains = true;
		}

		if (notContains) {
			TDNode* newNode = new TDNode();
			newNode->ParentDecomposition = ParentDecomposition;
			newNode->VertexByColor = std::vector<Vertex*>(VertexByColor.size());

			std::copy_if(Adjacent[i]->Bag.begin(), Adjacent[i]->Bag.end(), std::back_inserter(newNode->Bag),
				[this](Vertex* v){return this->BagContains(v);});
			for (Vertex* v : newNode->Bag)
				newNode->VertexByColor[v->Color] = v;

			newNode->Adjacent.push_back(this);
			newNode->Adjacent.push_back(Adjacent[i]);

			auto it = std::find(Adjacent[i]->Adjacent.begin(), Adjacent[i]->Adjacent.end(), this);
			int index = std::distance(Adjacent[i]->Adjacent.begin(), it);

			Adjacent[i]->Adjacent[index] = newNode;
			Adjacent[i] = newNode;

			ParentDecomposition->Nodes.push_back(newNode);
		}


		Adjacent[i]->PreJoinForget(this);
	}
}

bool TDNode::BagContains(Vertex* v) {
	return VertexByColor[v->Color] == v;
}

void TDNode::ForgetBeforeIntroduce(TDNode* parent) {
	std::vector<TDNode*> children;
	std::copy_if(Adjacent.begin(), Adjacent.end(), std::back_inserter(children),
		[parent](TDNode* i){return i != parent; });

	if (children.size() == 1) {
		TDNode* child = children.front();

		std::vector<Vertex*> toForget;
		std::copy_if(child->Bag.begin(), child->Bag.end(), std::back_inserter(toForget),
			[&](Vertex* v){ return !this->BagContains(v); });

		std::vector<Vertex*> toIntroduce;
		std::copy_if(this->Bag.begin(), this->Bag.end(), std::back_inserter(toIntroduce),
			[&](Vertex* v){ return !child->BagContains(v);});

		if (toForget.size() != 0 && toIntroduce.size() != 0) {
			TDNode* newNode = new TDNode();
			newNode->ParentDecomposition = ParentDecomposition;
			newNode->VertexByColor = std::vector<Vertex*>(VertexByColor.size());
			
			if (true) {
				std::vector<Vertex*> newNodeBag;
				std::copy_if(child->Bag.begin(), child->Bag.end(), std::back_inserter(newNodeBag),
					[&](Vertex* v){ return this->BagContains(v); });
				newNode->Bag = newNodeBag;
			}

			for (Vertex* v : newNode->Bag)
				newNode->VertexByColor[v->Color] = v;

			auto it = std::find(this->Adjacent.begin(), this->Adjacent.end(), child);
			int index = std::distance(this->Adjacent.begin(), it);

			this->Adjacent.erase(this->Adjacent.begin() + index);
			this->Adjacent.push_back(newNode);

			auto it2 = std::find(child->Adjacent.begin(), child->Adjacent.end(), this);
			int index2 = std::distance(child->Adjacent.begin(), it2);

			child->Adjacent.erase(child->Adjacent.begin() + index2);
			child->Adjacent.push_back(newNode);

			newNode->Adjacent.push_back(child);
			newNode->Adjacent.push_back(this);

			ParentDecomposition->Nodes.push_back(newNode);
			newNode->ForgetBeforeIntroduce(this);
			return;
		}

		child->ForgetBeforeIntroduce(this);
		return;
	}

	for (TDNode* child : children)
		child->ForgetBeforeIntroduce(this);
}

void TDNode::ColorVertices() {
	for (unsigned int i = 0; i < Bag.size(); i++) {
		VertexByColor[i] = Bag[i];
		Bag[i]->Color = i;
	}
	for (TDNode* nb : Adjacent)
		nb->ColorVertices(this);
}

void TDNode::ColorVertices(TDNode* parent) {
	for (unsigned int i = 0; i < Bag.size(); i++) {
		if (Bag[i]->Color >= 0)
			VertexByColor[Bag[i]->Color] = Bag[i];
	}

	int firstColor = 0;
	for (unsigned int i = 0; i < Bag.size(); i++) {
		if (Bag[i]->Color < 0) {
			while (VertexByColor[firstColor] != NULL) // null
				firstColor++;
			VertexByColor[firstColor] = Bag[i];
			Bag[i]->Color = firstColor;
		}
	}

	for (TDNode* nb : Adjacent) {
		if (nb != parent)
			nb->ColorVertices(this);
	}
}

void TDNode::DeconstructJoins(TDNode* parent) {
	std::vector<TDNode*> children;
	std::copy_if(Adjacent.begin(), Adjacent.end(), std::back_inserter(children),
		[parent](TDNode* i){return i != parent; });

	for (TDNode* child : children)
		child->DeconstructJoins(this);

	if (children.size() <= 2)
		return;

	std::vector < std::tuple<int, TDNode*> > toJoin;
	for (TDNode* child : children) {
		int code = 0;
		for (Vertex* v : child->Bag)
			code |= 1 << v->Color;
		toJoin.push_back(std::tuple<int, TDNode*>(code, child));

		auto it = std::find(child->Adjacent.begin(), child->Adjacent.end(), this);
		int index = std::distance(child->Adjacent.begin(), it);
		child->Adjacent.erase(child->Adjacent.begin() + index);
	}

	this->Adjacent.clear();
	this->Adjacent.shrink_to_fit();
	Adjacent.push_back(parent);

	while (toJoin.size() > 2 || (toJoin.size() == 2 && BitCount(std::get<0>(toJoin[0]) | std::get<0>(toJoin[1])) < Bag.size()))  {
		int min = INT_MAX;
		std::tuple<int, TDNode*> left;
		std::tuple<int, TDNode*> right;
		for (unsigned int i = 0; i < toJoin.size(); i++) {
			for (unsigned int j = i + 1; j < toJoin.size(); j++) {
				if (BitCount(std::get<0>(toJoin[i]) | std::get<0>(toJoin[j])) < min) {
					min = BitCount(std::get<0>(toJoin[i]) | std::get<0>(toJoin[j]));
					left = toJoin[i];
					right = toJoin[j];
				}
			}
		}

		auto it = std::find(toJoin.begin(), toJoin.end(), left);
		int index = std::distance(toJoin.begin(), it);
		toJoin.erase(toJoin.begin() + index);

		auto it2 = std::find(toJoin.begin(), toJoin.end(), right);
		int index2 = std::distance(toJoin.begin(), it2);
		toJoin.erase(toJoin.begin() + index2);

		TDNode* newNode = new TDNode();
		newNode->ParentDecomposition = ParentDecomposition;
		newNode->VertexByColor = std::vector<Vertex*>(VertexByColor.size());

		toJoin.push_back(std::tuple<int, TDNode*>(std::get<0>(left) | std::get<0>(right), newNode));

		newNode->Adjacent.push_back(std::get<1>(left));
		newNode->Adjacent.push_back(std::get<1>(right));
		std::get<1>(left)->Adjacent.push_back(newNode);
		std::get<1>(right)->Adjacent.push_back(newNode);

		ParentDecomposition->Nodes.push_back(newNode);

		for (unsigned int i = 0; i < VertexByColor.size(); i++) {
			if (std::get<1>(left)->VertexByColor[i] != NULL) {
				newNode->Bag.push_back( std::get<1>(left)->VertexByColor[i]);
				newNode->VertexByColor[i] = std::get<1>(left)->VertexByColor[i];
			}
			else if (std::get<1>(right)->VertexByColor[i] != NULL) {
				newNode->Bag.push_back( std::get<1>(right)->VertexByColor[i]);
				newNode->VertexByColor[i] = std::get<1>(right)->VertexByColor[i];
			}
		}
	}

	this->Adjacent.push_back(std::get<1>(toJoin[0]));
	std::get<1>(toJoin[0])->Adjacent.push_back(this);

	if (toJoin.size() > 1) {
		this->Adjacent.push_back(std::get<1>(toJoin[1]));
		std::get<1>(toJoin[1])->Adjacent.push_back(this);
	}
}

int TDNode::BitCount(int i) {
	i = i - ((i >> 1) & 0x55555555);
	i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
	return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
}

void TDNode::Compute(std::vector<Instruction*>* traversal, TDNode* parent) {
	std::vector<TDNode*> children;
	std::copy_if(Adjacent.begin(), Adjacent.end(), std::back_inserter(children),
		[parent](TDNode* i){return (i != parent && i->Subtree.size() > 0); });

	if (children.size() == 0) {
		Instruction* leaf = new Instruction(Instruction::Leaf, {}, false);
		traversal->push_back(leaf);
		ComputeIntroduce(this, NULL, Bag, traversal);
		return;
	}

	if (children.size() == 1) {
		TDNode* child = children.front();

		std::vector<Vertex*> toForget;
		std::copy_if(child->Bag.begin(), child->Bag.end(), std::back_inserter(toForget),[&](Vertex* v){ 
			return !this->BagContains(v);
		});
		std::sort(toForget.begin(), toForget.end(), [&](Vertex* i, Vertex* j){
			std::vector<Edge*> vectorI;
			std::vector<Edge*> vectorJ;
			std::copy_if(i->Adjacent.begin(), i->Adjacent.end(), std::back_inserter(vectorI), [&](Edge* v){
				return BagContains(v->To);
			});
			std::copy_if(j->Adjacent.begin(), j->Adjacent.end(), std::back_inserter(vectorI), [&](Edge* v){
				return BagContains(v->To);
			});

			return vectorI.size() < vectorJ.size();
		});
		std::reverse(toForget.begin(), toForget.end());

		std::vector<Vertex*> toIntroduce;
		std::copy_if(this->Bag.begin(), this->Bag.end(), std::back_inserter(toIntroduce),
			[&](Vertex* v){ return !child->BagContains(v); });

		if (toForget.size() > 0 && toIntroduce.size() > 0)
			throw "error";

		if (toIntroduce.size() > 0) {
			ComputeIntroduce(this, child, toIntroduce, traversal);
			return;
		}

		if (toForget.size() > 0) {
			ComputeForget(this, child, toForget, traversal);
			return;
		}

		// No change?
		child->Compute(traversal, this);
		return;
	}

	if (children.size() > 2)
		throw "TD not preprocessed!";

	TDNode* left = children[0];
	TDNode* right = children[1];
	ComputeJoin(this, left, right, traversal);
}

void TDNode::IntroduceIfNecessary(std::vector<Instruction*>* traversal, TDNode* child) {
	std::vector<Vertex*> toIntroduce;
	std::copy_if(Bag.begin(), Bag.end(), std::back_inserter(toIntroduce),
		[child](Vertex* v){ return !child->BagContains(v); });
	if (toIntroduce.size() > 0)
		ComputeIntroduce(this, NULL, toIntroduce, traversal);
}

void TDNode::ComputeIntroduce(TDNode* bag, TDNode* child, std::vector<Vertex*> vertices, std::vector<Instruction*>* traversal) {
	if (child != NULL)
		child->Compute(traversal, this);
	for (Vertex* v : vertices) { // sort vertices?
		std::vector<Vertex*> toIntroduce = traversal->back()->bag;
		toIntroduce.push_back(v);
		std::sort(toIntroduce.begin(), toIntroduce.end(), [](Vertex* i, Vertex* j){return i->id < j->id; });
		
		Instruction* introduceBag = new Instruction(Instruction::Introduce, toIntroduce, true, v->id);
		traversal->push_back(introduceBag);
		ComputeIntroduceEdge(v, traversal);
	}
}
void TDNode::ComputeIntroduceEdge(Vertex* vertex, std::vector<Instruction*>* traversal) {
	Instruction* introduceBag = traversal->back();
	std::vector<Edge*> introEdges;
	for (Edge* e : vertex->Adjacent){
		if (std::find(introduceBag->bag.begin(), introduceBag->bag.end(), e->To) != introduceBag->bag.end() &&
			std::find(introEdges.begin(), introEdges.end(), e) == introEdges.end()) {
			introEdges.push_back(e);
			Instruction* introduceEdgeBag = new Instruction(Instruction::IntroduceEdge, introduceBag->bag, true, e->From->id, e->To->id);
			traversal->push_back(introduceEdgeBag);
			// todo check if not added multiple times
			// check if needs to be sorted
		}
	}
}

bool operator == (const Edge &L, const Edge &R) {
	if (L.From->id == R.From->id &&
		L.To->id == R.To->id)
		return true;
	return false;
}

void TDNode::ComputeForget(TDNode* bag, TDNode* child, std::vector<Vertex*> vertices, std::vector<Instruction*>* traversal) {
	if (child != NULL)
		child->Compute(traversal, this);
	for (Vertex* v : vertices) {
		std::vector<Vertex*> toDelete = traversal->back()->bag;

		auto it = std::find(toDelete.begin(), toDelete.end(), v);
		int index = std::distance(toDelete.begin(), it);
		toDelete.erase(toDelete.begin() + index);

		Instruction* forgetBag = new Instruction(Instruction::Forget, toDelete, true, v->id);
		traversal->push_back(forgetBag);
	}
}

void TDNode::ComputeJoin(TDNode* bag, TDNode* left, TDNode* right, std::vector<Instruction*>* traversal) {
	left->Compute(traversal, this);
	IntroduceIfNecessary(traversal, left);
	right->Compute(traversal, this);
	IntroduceIfNecessary(traversal, right);
	Instruction* joinBag = new Instruction(Instruction::Join, traversal->back()->bag);
	traversal->push_back(joinBag);
}
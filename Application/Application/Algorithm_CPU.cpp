#include "stdafx.h"
#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>
#include "Algorithm_CPU.h"
#include "Timer.h"
#include "stdafx.h"
#include <algorithm>
#include <string>

#include "Output.h"
#include "Global.h"

bool checkEmpty(const std::string& filename) {
	std::ifstream ifile(filename.c_str());
	return ifile.peek() == std::ifstream::traits_type::eof();
}

std::pair<int, double> Algorithm_CPU::runAlgorithm(TreeDecomposition* td) {

	std::vector<std::vector<std::vector<int>>>joindata;
	std::vector<std::vector<int>> indexLocations;
	std::vector<std::vector<int>> joinDataPerWidth;
	joinDataPerWidth = std::vector<std::vector<int>>();
	indexLocations = std::vector<std::vector<int>>();

	for (int i = 0; i <= td->width; i++) { // width
		joindata.push_back(std::vector<std::vector<int>>());
		indexLocations.push_back(std::vector<int>());
		for (int j = 0; j < sizeOfStepOrArray(i); j++) { // index
			joindata[i].push_back(std::vector<int>());
			joindata[i][j].push_back(-1);
			joindata[i][j].push_back(-1);
			int zeroCount = 0;
			int zeroMask = j;

			for (int k = 0; k < i; k++) { // position in index
				int value = TernaryOperations::valueAtIndex(j, k);
				if (value == 0) {
					zeroCount++;
					joindata[i][j].push_back(k);
					zeroMask += TernaryOperations::bitShiftLeft(1, k);
				}
			}

			joindata[i][j][0] = zeroCount;
			joindata[i][j][1] = zeroMask;
		}
	}

	for (int i = 0; i <= td->width; i++) {
		for (int j = 0; j < sizeOfStepOrArray(i); j++) {
			for (int Z = 0; Z < pow(2, joindata[i][j][0]); Z++) {
				int copyRight = joindata[i][j][1];
				int copyLeft = j;

				for (int k = 0; k < i; k++) {
					int value = Z >> k;
					int bitmask = 1;
					value = value & bitmask;

					if (value == 1) {
						copyLeft += TernaryOperations::bitShiftLeft(1, joindata[i][j][k + 2]);
						copyRight -= TernaryOperations::bitShiftLeft(1, joindata[i][j][k + 2]);
					}
				}
				joindata[i][j].push_back(copyLeft);
				joindata[i][j].push_back(copyRight);
			}
		}
	}

	for (unsigned int i = 0; i < joindata.size(); i++) {
		joinDataPerWidth.push_back(std::vector<int>());
		for (int j = 0; j < sizeOfStepOrArray(i); j++) {
			indexLocations[i].push_back(joinDataPerWidth[i].size());
			joinDataPerWidth[i].insert(joinDataPerWidth[i].end(), joindata[i][j].begin(), joindata[i][j].end());
		}
	}

	this->joinDataPerWidth = joinDataPerWidth;
	this->indexLocations = indexLocations;


	Timer timer;
	timer.start();

	this->oldValues.push_back({ -1 });

	int index1InBag, index2InBag, indexInPreviousBag;
	int node1;
	int node2;
	int answer = -1;

	for (size_t i = 0; i < traversal.size(); i++) {
		Instruction* instruction = traversal[i];
		node1 = instruction->getNode1();
		node2 = instruction->getNode2();
		this->previousBags.push_back(this->currentBag);
		this->currentBag = traversal[i]->bag;
		std::vector<Vertex*> previousBag = this->previousBags.back();

		unsigned long sizeThisStep = sizeOfStepOrArray(this->currentBag.size());

		switch (instruction->getInstructionType()) {
		case Instruction::Leaf:
			for (unsigned long index = 0; index < sizeThisStep; index++)
				this->newValues.push_back(leafBag());
			break;
		case Instruction::Introduce:
			index1InBag = std::distance(this->currentBag.begin(), std::find_if(this->currentBag.begin(), this->currentBag.end(), [node1](Vertex* v) {return v->id == node1; }));

			if (node1 != currentBag.back()->id)
				for (unsigned long index = 0; index < sizeThisStep; index++)
					this->newValues.push_back(introduceBag(index, index1InBag, false));
			else
				for (unsigned long index = 0; index < sizeThisStep; index++)
					this->newValues.push_back(introduceBag(index, index1InBag));
			break;
		case Instruction::IntroduceEdge:
			index1InBag = std::distance(this->currentBag.begin(), std::find_if(this->currentBag.begin(), this->currentBag.end(), [node1](Vertex* v) {return v->id == node1; }));
			index2InBag = std::distance(this->currentBag.begin(), std::find_if(this->currentBag.begin(), this->currentBag.end(), [node2](Vertex* v) {return v->id == node2; }));
			for (unsigned long index = 0; index < sizeThisStep; index++)
				this->newValues.push_back(introduceEdgeBag(index, index1InBag, index2InBag));
			break;
		case Instruction::Forget:
			indexInPreviousBag = std::distance(previousBag.begin(), std::find_if(previousBag.begin(), previousBag.end(), [node1](Vertex* v) {return v->id == node1; }));
			for (unsigned long index = 0; index < sizeThisStep; index++)
				this->newValues.push_back(forgetBag(index, indexInPreviousBag, (1 << this->currentBag.size())));
			break;
		case Instruction::Join:
			for (unsigned long index = 0; index < sizeThisStep; index++)
				this->newValues.push_back(joinBag(index, currentBag.size()));
			break;
		case Instruction::Empty:
			answer = oldValues.back()[0];
			break;
		}

		if (traversal[i]->prevIsChild) {
			if (instruction->getInstructionType() == Instruction::Join) {
				oldValues.pop_back();
				oldValues.pop_back();
				previousBags.pop_back();
				previousBags.pop_back();
			}
			else {
				oldValues.pop_back();
				previousBags.pop_back();
			}
		}

		this->oldValues.push_back(newValues);
		this->newValues.clear();
	}

	timer.stop();

	return std::pair<int, double>(answer, timer.getTime());
}


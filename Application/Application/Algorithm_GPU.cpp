#include "stdafx.h"
#include <iostream>
#include <string>
#include <algorithm>
#include <iostream>

#include "Algorithm_GPU.h"
#include "TernaryOperations.h"

long arraySize;
short *answer;
double used = 0;
int freeSize = 0;

void memoryUsed() {
	cudaError_t cudaStatus;
	size_t free_byte;
	size_t total_byte;
	cudaStatus = cudaMemGetInfo(&free_byte, &total_byte);

	double free_db = (double)free_byte;
	double total_db = (double)total_byte;
	double used_db = total_db - free_db;
	if (used != used_db / 1024.0 / 1024.0) {
		used = used_db / 1024.0 / 1024.0;
		std::cout << "                                       " << "used: " << std::to_string(used_db / 1024.0 / 1024.0) << "\n";
	}

	//std::cout << "used: " << std::to_string(used_db / 1024.0 / 1024.0) << "\n";
	//std::cout << "free: " << std::to_string(free_db / 1024.0 / 1024.0) << "\n";
	//std::cout << "total: " << std::to_string(total_db / 1024.0 / 1024.0) << "\n";
}

std::pair<int, double> Algorithm_GPU::runAlgorithm(TreeDecomposition* td) {

	std::vector<std::vector<std::vector<int>>>joindata;
	std::vector<std::vector<int>> indexLocations;
	std::vector<std::vector<int>> joinDataPerWidth = std::vector<std::vector<int>>();

	for (int i = 0; i <= td->width; i++) { // width
		joindata.push_back(std::vector<std::vector<int>>());
		indexLocations.push_back(std::vector<int>());
		for (int j = 0; j < sizeOfStepOrArray(i); j++) { // index
			joindata[i].push_back(std::vector<int>());
			joindata[i][j].push_back(-1);
			joindata[i][j].push_back(-1);
			int zeroCount = 0;
			int zeroMask = j;

			for (int k = 0; k < i; k++) { // position in index
				int value = TernaryOperations::valueAtIndex(j, k);
				if (value == 0) {
					zeroCount++;
					joindata[i][j].push_back(k);
					zeroMask += TernaryOperations::bitShiftLeft(1, k);
				}
			}
			
			joindata[i][j][0] = zeroCount;
			joindata[i][j][1] = zeroMask;
		}
	}

	for (int i = 0; i <= td->width; i++) {
		for (int j = 0; j < sizeOfStepOrArray(i); j++) {
			for (int Z = 0; Z < pow(2, joindata[i][j][0]); Z++) {
				int copyRight = joindata[i][j][1];
				int copyLeft = j;

				for (int k = 0; k < i; k++) {
					int value = Z >> k;
					int bitmask = 1;
					value = value & bitmask;

					if (value == 1) {
						copyLeft += TernaryOperations::bitShiftLeft(1, joindata[i][j][k + 2]);
						copyRight -= TernaryOperations::bitShiftLeft(1, joindata[i][j][k + 2]);
					}
				}
				joindata[i][j].push_back(copyLeft);
				joindata[i][j].push_back(copyRight);
			}
		}
	}

	for (unsigned int i = 0; i < joindata.size(); i++) {
		joinDataPerWidth.push_back(std::vector<int>());
		for (int j = 0; j < sizeOfStepOrArray(i); j++) {
			indexLocations[i].push_back(joinDataPerWidth[i].size());
			joinDataPerWidth[i].insert(joinDataPerWidth[i].end(), joindata[i][j].begin(), joindata[i][j].end());
		}
	}

	for (unsigned int i = 0; i < indexLocations.size(); i++) {
		int* newIndexPointer = 0;
		int* newJoinDataPointer = 0;

		cudaError_t cudaStatus;

		cudaStatus = cudaMalloc((void**)&newIndexPointer, indexLocations[i].size() * sizeof(int));
		indexLocationPointers.push_back(newIndexPointer);
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "cudaMalloc failed!  newValues in AllocateMemory \n");
			freeAllMemory();
			return std::pair<int,double>(-1, -1.0);
		}

		cudaStatus = cudaMemcpy(newIndexPointer, indexLocations[i].data(), indexLocations[i].size() * sizeof(int), cudaMemcpyHostToDevice);
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "cudaMemcpy failed! [nodeIndex]\n");
			freeAllMemory();
			return std::pair<int, double>(-1, -1.0);
		}


		cudaStatus = cudaMalloc((void**)&newJoinDataPointer, joinDataPerWidth[i].size() * sizeof(int));
		joinDataPerWidthPointers.push_back(newJoinDataPointer);
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "cudaMalloc failed!  newValues in AllocateMemory \n");
			freeAllMemory();
			return std::pair<int, double>(-1, -1.0);
		}

		cudaStatus = cudaMemcpy(newJoinDataPointer, joinDataPerWidth[i].data(), joinDataPerWidth[i].size() * sizeof(int), cudaMemcpyHostToDevice);
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "cudaMemcpy failed! [nodeIndex]\n");
			freeAllMemory();
			return std::pair<int, double>(-1, -1.0);
		}

		//short stuff[100];
		//cudaStatus = cudaMemcpy(&stuff, newJoinDataPointer, joinDataPerWidth[i].size() * sizeof(short), cudaMemcpyDeviceToHost);
		//int k = 0;
		//cudaStatus = cudaMemcpy(stuff, valuePointers.back(), newSize * sizeof(short), cudaMemcpyDeviceToHost);

	}

	Timer timer;
	timer.start();


	// largest size of a bag
	arraySize = sizeOfStepOrArray(td->width); 
	// std::cout << "bagsize: " << arraySize << " width: " << td->width << "\n";
	// pointer to memory, this is needed for cuda memory copy back to memory
	short x = -1;
	answer = &x;
	// node[0] = instruction node 1 
	// node[1] = index of node 1
	// node[2] = instruction node 2
	// node[3] = index of node 2
	int node[4];
	// initialize CUDA
	cudaInit();

	this->sizeThisStep = 1;
	cudaError cudaStatus;

	// Compute the values for each bag
	for (size_t i = 0; i < traversal.size(); i++) {
		Instruction* instruction = traversal[i];
		node[0] = instruction->getNode1();
		node[2] = instruction->getNode2();

		this->previousBags.push_back(this->currentBag);
		this->currentBag = traversal[i]->bag;
		std::vector<Vertex*> previousBag = this->previousBags.back();

		// get the previous and current bag and their step sizes for both
		this->sizePreviousStep = this->sizeThisStep; 
		this->sizeThisStep = sizeOfStepOrArray(currentBag.size());

		// if pointer position and valuepointer.length is same, add new memory
		if (newPointerPosition == valuePointers.size() && instruction->getInstructionType() != Instruction::Empty) {
			bool found = false;

			if (!found) {
				int* newValuePointer = 0;
				long bagSize = 0;

				// always allocate the largest arraySize
				if (Global::staticBagSize)
					bagSize = arraySize;
				else
					bagSize = sizeOfStepOrArray(currentBag.size());

				cudaStatus = allocateMemory(newValuePointer, bagSize);
				if (cudaStatus != cudaSuccess) {
					fprintf(stderr, "Allocating new memory failed, check the size of the allocated memory");
					break;
				}
			}
		}

		// execute one of the five functions based on instruction type
		switch (instruction->getInstructionType()) {
		case Instruction::Leaf:
			cudaStatus = cudaOperation(node, this->sizeThisStep, Instruction::Leaf, traversal[i]->prevIsChild, currentBag.size());
			break;

		case Instruction::Introduce: {
			int node0 = node[0];
			node[1] = std::distance(this->currentBag.begin(), std::find_if(this->currentBag.begin(), this->currentBag.end(), [node0](Vertex* v) {return v->id == node0; }));
			if (node[0] != currentBag.back()->id)
				cudaStatus = cudaOperation(node, this->sizeThisStep, Instruction::Introduce, traversal[i]->prevIsChild, currentBag.size(), false);
			else 
				cudaStatus = cudaOperation(node, this->sizeThisStep, Instruction::Introduce, traversal[i]->prevIsChild, currentBag.size());
			break;
			}

		case Instruction::IntroduceEdge: {
			int node0 = node[0];
			int node2 = node[2];
			node[1] = std::distance(this->currentBag.begin(), std::find_if(this->currentBag.begin(), this->currentBag.end(), [node0](Vertex* v) {return v->id == node0; }));
			node[3] = std::distance(this->currentBag.begin(), std::find_if(this->currentBag.begin(), this->currentBag.end(), [node2](Vertex* v) {return v->id == node2; }));
			cudaStatus = cudaOperation(node, this->sizeThisStep, Instruction::IntroduceEdge, traversal[i]->prevIsChild, currentBag.size());
			break;
		}

		case Instruction::Forget: {
			int node0 = node[0];
			node[1] = std::distance(previousBag.begin(), std::find_if(previousBag.begin(), previousBag.end(), [node0](Vertex* v) {return v->id == node0; }));
			cudaStatus = cudaOperation(node, this->sizeThisStep, Instruction::Forget, traversal[i]->prevIsChild, currentBag.size());
			break;
		}

		case Instruction::Join:
			cudaStatus = cudaOperation(node, this->sizeThisStep, Instruction::Join, traversal[i]->prevIsChild, currentBag.size());
			break;

		case Instruction::Empty:
			break;
		}

		if (cudaStatus != cudaSuccess)
			break;
	}
	if (cudaStatus == cudaSuccess)
		cudaFinish();

	timer.stop();

	return std::pair<int, double>(*answer, timer.getTime());
}

// Deallocates all allocated memory
void Algorithm_GPU::freeAllMemory() {
	cudaFree(dev_nodeIndex);
	for (int* address: valuePointers)
		cudaFree(address);
	for (int* address : indexLocationPointers)
		cudaFree(address);
	for (int* address : joinDataPerWidthPointers)
		cudaFree(address);
}

// Deallocates the memory of the gives memory address
void Algorithm_GPU::freeMemory(int* address) {
	cudaFree(address);
}

// Initializes the the gpu and node memory allocation
cudaError_t Algorithm_GPU::cudaInit() {
	cudaError_t cudaStatus;

	// Choose which GPU to run on, change this on a multi-GPU system.
	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?\n");
		freeAllMemory();
	}

	// contains the node 
	cudaStatus = cudaMalloc((void**)&dev_nodeIndex, 4 * sizeof(int));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed! nodeIndex in CudaInit \n");
		freeAllMemory();
	}

	return cudaStatus;
}

// Allocate memory for a single bag
cudaError_t Algorithm_GPU::allocateMemory(int* memoryLocation, long bagSize) {
	cudaError_t cudaStatus;

	//std::cout << "allocating: " << std::to_string(bagSize * sizeof(short) / 1024.0 / 1024.0) << "\n";
	// Allocate memory on the GPU for the new values based on the largest bag size
	cudaStatus = cudaMalloc((void**)&memoryLocation, bagSize * sizeof(int));
	valuePointers.push_back(memoryLocation);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!  newValues in AllocateMemory \n");
		freeAllMemory();
		return cudaStatus;
	}
	pointerSizes.push_back(bagSize);
	return cudaStatus;
}

// Is called when the algorithm has traversed the tree decomposition
cudaError_t Algorithm_GPU::cudaFinish() {
	cudaError_t cudaStatus;

	// Copy output vector from GPU buffer to host memory.
	cudaStatus = cudaMemcpy(answer, valuePointers.back(), sizeof(short), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed! [newValues]\n");
		freeAllMemory();
	}
	return cudaStatus;
}

// Passes the needed node details to the GPU, launches the kernel, waits for a 
// result and dealocates memory when needed
cudaError_t Algorithm_GPU::cudaOperation(const int *nodeIndex, 
	unsigned int newSize, Instruction::InstructionType instructionType, bool prevIsChild, int bagSize, bool isLast){
	cudaError_t cudaStatus;


	// Copy node details to GPU
	cudaStatus = cudaMemcpy(dev_nodeIndex, nodeIndex, 4 * sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed! [nodeIndex]\n");
		freeAllMemory();
		return cudaStatus;
	}
	//memoryUsed();

	//Launch kernel
	cudaStatus = launchKernel(newSize, instructionType, bagSize, isLast);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		freeAllMemory();
		return cudaStatus;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		std::string check = cudaGetErrorString(cudaStatus);
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching kernel!\n", cudaStatus);
		std::cout << cudaGetErrorName(cudaStatus) << "\n";
		std::cout << cudaGetErrorString(cudaStatus) << "\n";
		std::cout << cudaGetLastError() << "\n";
		freeAllMemory();
		return cudaStatus;
	}


	//Debugging
	//short stuff[1000];
	//cudaStatus = cudaMemcpy(&stuff, valuePointers.back(), newSize * sizeof(short), cudaMemcpyDeviceToHost);
	//int i = 0;
	//cudaStatus = cudaMemcpy(stuff, valuePointers.back(), newSize * sizeof(short), cudaMemcpyDeviceToHost);


	//memoryUsed();


	// release memory from old locations that we no longer need 
	// while keeping the pointer to the new values
	int* newPointer = valuePointers.back();
	valuePointers.pop_back();
	long currentSize = pointerSizes.back();
	pointerSizes.pop_back();

	if (prevIsChild) {
		if (instructionType == Instruction::Join) {
			// save the second last element
			if (!Global::dealocMem) {
				freeMemory(valuePointers.back());
				valuePointers.pop_back();
				freeMemory(valuePointers.back());
				valuePointers.pop_back();
				newPointerPosition -= 2;
			}
			else {
				notUsedMemory[pointerSizes.back()].push_back(valuePointers.back());
				valuePointers.pop_back();
				freeSize += pointerSizes.back();
				pointerSizes.pop_back();
				notUsedMemory[pointerSizes.back()].push_back(valuePointers.back());
				valuePointers.pop_back();
				freeSize += pointerSizes.back();
				pointerSizes.pop_back();
			}
			previousBags.pop_back();
			previousBags.pop_back();
		}
		else {
			if (!Global::dealocMem) {
				freeMemory(valuePointers.back());
				valuePointers.pop_back();
				newPointerPosition -= 1;
			}
			else {
				notUsedMemory[pointerSizes.back()].push_back(valuePointers.back());
				valuePointers.pop_back();
				freeSize += pointerSizes.back();
				pointerSizes.pop_back();
			}
			previousBags.pop_back();
		}
	}

	valuePointers.push_back(newPointer);
	pointerSizes.push_back(currentSize);
	newPointerPosition++;
	return cudaStatus;
}
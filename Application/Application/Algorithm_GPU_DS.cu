#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>

#include "Algorithm_GPU_DS.cuh"
#include "TernaryOperations.h"


__device__ int bitShiftLeft(int toShift, int placesToShift) {
	int result = toShift;
	for (int i = 0; i < placesToShift; i++)
		result *= 3;
	return result;
}

__device__ int bitShiftRight(int toShift, int placesToShift) {
	int result = toShift;
	for (int i = 0; i < placesToShift; i++)
		result /= 3;
	return result;
}

__device__ int valueAtIndex(int number, int index) {
	int newNumber = bitShiftRight(number, index);
	int value = newNumber % 3;
	return value;
}

__device__ int removeValueAtIndex(int number, int index, int value) {
	int removeValue = bitShiftLeft(1, index) * value;
	return number - removeValue;
}

Algorithm_GPU_DS::Algorithm_GPU_DS(std::vector<Instruction*> traversal) {
	this->traversal = traversal;
}

int Algorithm_GPU_DS::sizeOfStepOrArray(int exponent) {
	return TernaryOperations::bitShiftLeft(1, exponent);
}

__global__ void leafBagKernelDS(int *newValues) {
	int index = (blockIdx.x * blockDim.x) + threadIdx.x;
	newValues[index] = 0;
}

__global__ void introduceBagKernelDS(int *newValues, int *nodeIndex, int *oldValues, int indicesPerThread, bool isLast = true) {
	for (int i = 0; i < indicesPerThread; i++) {
		int index = (blockIdx.x * blockDim.x) + (threadIdx.x * indicesPerThread) + i;	
		int colour = valueAtIndex(index, nodeIndex[1]);

		newValues[index] = colour;

		if (colour == 0)
			newValues[index] = 10000;
		else if (isLast) {
			int newIndex = removeValueAtIndex(index, nodeIndex[1], colour);
			newValues[index] = oldValues[newIndex];
		}
		else {
			int leftSide = bitShiftRight(index, nodeIndex[1]);
			leftSide = bitShiftLeft(leftSide, nodeIndex[1]);
			int rightSide = index - leftSide;
			leftSide = bitShiftRight(leftSide, nodeIndex[1] + 1);
			leftSide = bitShiftLeft(leftSide, nodeIndex[1]);

			newValues[index] = oldValues[leftSide + rightSide];
		}
	}
}

__global__ void introduceEdgeBagKernelDS(int *newValues, int *nodeIndex, int *oldValues, int indicesPerThread) {
	for (int i = 0; i < indicesPerThread; i++) {
		int index = (blockIdx.x * blockDim.x) + (threadIdx.x * indicesPerThread) + i;

		int node1Value = valueAtIndex(index, nodeIndex[1]);
		int node2Value = valueAtIndex(index, nodeIndex[3]);

		if ((node1Value == 2 && node2Value == 0) || (node1Value == 0 && node2Value == 2)) {
			if (node1Value == 0) {
				int positionMask1 = bitShiftLeft(1, nodeIndex[1]);
				newValues[index] = oldValues[index + positionMask1];
			}
			else {
				int positionMask2 = bitShiftLeft(1, nodeIndex[3]);
				newValues[index] =  oldValues[index + positionMask2];
			}
		}
		else
			newValues[index] = oldValues[index];
	}
}

__global__ void forgetBagKernelDS(int *newValues, int *nodeIndex, int *oldValues, int indicesPerThread) {
	for (int i = 0; i < indicesPerThread; i++) {
		int index = (blockIdx.x * blockDim.x) + (threadIdx.x * indicesPerThread) + i;
		
		int positionMask = bitShiftLeft(1, nodeIndex[1]);

		int leftSide = bitShiftLeft(bitShiftRight(index, nodeIndex[1]), nodeIndex[1]);
		int rightSide = index - leftSide;
		int combined = bitShiftLeft(leftSide, 1) + rightSide;

		int option1Index = combined;
		int option2Index = combined + positionMask * 2;

		int option1 = oldValues[option1Index];
		int option2 = oldValues[option2Index] + 1;

		if (option1 > option2)
			newValues[index] = option2;
		else
			newValues[index] = option1;
	}
}

__global__ void joinBagKernelDS(int *newValues, int *nodeIndex, int *oldValues1, int *oldValues2, int indicesPerThread, unsigned int newSize, int bagSize, int* indices, int* joinData) {
	for (int i = 0; i < indicesPerThread; i++) {
		int index = (blockIdx.x * blockDim.x) + (threadIdx.x * indicesPerThread) + i;
		int answer = 10000;

		int joinIndex = indices[index];
		int zeroCount = joinData[joinIndex];

		for (int j = 0; j < (1 << zeroCount); j++) {
			int indexLeft = joinIndex + 2 + zeroCount + (j * 2);
			//printf("indexLeft: %i \n", indexLeft);

			int left = joinData[indexLeft];
			int right = joinData[indexLeft + 1];

			//printf("left: %i \n", left);
			int old = oldValues1[left] + oldValues2[right];

			if (old < answer) {
				answer = old;
			}
		}
		newValues[index] = answer;
	}
}



//__global__ void joinBagKernelDS(int *newValues, int *nodeIndex, int *oldValues1, int *oldValues2, int indicesPerThread, unsigned int newSize, int bagSize) {
//	for (int i = 0; i < indicesPerThread; i++) {
//		int index = (blockIdx.x * blockDim.x) + (threadIdx.x * indicesPerThread) + i;
//
//		int answer = 5000;
//		int k = 0;
//		int* zeroPositions = new int[bagSize];
//		int position = 0;
//		int leftValue = index;
//		int rightValue = index;
//
//		// Compute bitmask a, bitmask b, k and save the zero positions
//		for (int j = 0; j < bagSize; j++) {
//			int value = valueAtIndex(index, j); // get the value at position i in the index
//			if (value == 0) {
//				zeroPositions[position] = j;
//				position++;
//				rightValue += bitShiftLeft(1, j);
//				k++;
//			}
//		}
//
//		// loop over the amount of locations for 0
//		for (int Z = 0; Z < (1 << k); Z++) {
//			int copyRight = rightValue;
//			int copyLeft = leftValue;
//
//			for (int j = 0; j < bagSize; j++) {
//				int value = Z >> j;
//				int bitmask = 1;
//				value = value & bitmask;
//
//				if (value == 1) {
//					copyLeft += bitShiftLeft(1, zeroPositions[j]);
//					copyRight -= bitShiftLeft(1, zeroPositions[j]);
//				}
//			}
//
//			int old = oldValues1[copyLeft] + oldValues2[copyRight];
//			if (old < answer)
//				answer = old;
//		}
//
//		delete zeroPositions;
//		newValues[index] = answer;
//	}
//}

cudaError_t Algorithm_GPU_DS::launchKernel(unsigned int newSize, Instruction::InstructionType instructionType, int bagSize, bool isLast) {
	cudaError_t cudaStatus;

	std::pair<int, int> kernelParams = getKernelParameters(newSize);
	int blocks = kernelParams.first;
	int threads = kernelParams.second;

	int* dev_newValues = valuePointers.back();
	int* dev_oldValues;
	if (instructionType != Instruction::Leaf)
		dev_oldValues = valuePointers.end()[-2];

	if (instructionType == Instruction::Leaf) {
		leafBagKernelDS << <blocks, threads >> >(dev_newValues);
	}
	else if (instructionType == Instruction::Introduce) {
		introduceBagKernelDS << <blocks, threads >> >(dev_newValues, dev_nodeIndex, dev_oldValues, Global::indicesPerThread, isLast);
	}
	else if (instructionType == Instruction::IntroduceEdge) {
		introduceEdgeBagKernelDS << <blocks, threads >> >(dev_newValues, dev_nodeIndex, dev_oldValues, Global::indicesPerThread);
	}
	else if (instructionType == Instruction::Forget) {
		forgetBagKernelDS << <blocks, threads >> >(dev_newValues, dev_nodeIndex, dev_oldValues, Global::indicesPerThread);
	}
	else if (instructionType == Instruction::Join) {
		int* dev_oldValues2 = valuePointers.end()[-3];
		joinBagKernelDS << <blocks, threads >> >(dev_newValues, dev_nodeIndex, dev_oldValues, dev_oldValues2, Global::indicesPerThread, newSize, bagSize, indexLocationPointers[bagSize], joinDataPerWidthPointers[bagSize]);
	}
	cudaStatus = cudaGetLastError();
	return cudaStatus;
}


std::pair<int, int> Algorithm_GPU_DS::getKernelParameters(unsigned int newSize) {
	int blocks;
	int threads;

	if (newSize <= Global::indicesPerThread) {
		blocks = 1;
		threads = 1;
	}
	else {
		int numberOfThreadsNecessary = ceil(newSize / Global::indicesPerThread);
		int index = 0;
		for (int i = 0; i < 30; i++) {
			if (newSize == TernaryOperations::bitShiftLeft(1, i)) {
				index = i;
				break;
			}
		}

		if (index <= 6) {
			blocks = 1;
			threads = numberOfThreadsNecessary;
		}
		else {
			blocks = TernaryOperations::bitShiftLeft(1, (index - 6));
			threads = 729;
		}
	}
	return std::pair<int, int>(blocks, threads);
}

#pragma once
#ifndef GLOBAL_H
#define GLOBAL_H

namespace Global {
	// Algorithm
	extern const bool useGPU;
	extern const bool useMIS;
	extern const bool useIntroduceEdge;
	extern const bool multipleRuns;
	extern const bool staticBagSize;
	extern const bool dealocMem;
	extern const int numberOfRuns;
	extern const int numberOfGraphs;
	extern const int minValue;
	extern const int maxValue;
	extern const unsigned int indicesPerThread;
}

#endif
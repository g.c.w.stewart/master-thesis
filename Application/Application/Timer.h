#pragma once

class Timer {
public:
	Timer();
	void start(); // start timer
	void stop(); // stop timer
	void reset(); // reset timer
	bool isRunning(); // checks if the timer is running
	double getTime(); // returns the current Time or the time of the run based on isRunning()
	bool isOver(unsigned long seconds);
private:
	bool resetted;
	bool running;
	unsigned long beg;
	unsigned long end;
};
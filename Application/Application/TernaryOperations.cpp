#include "stdafx.h"
#include "TernaryOperations.h"

int TernaryOperations::bitShiftLeft(int toShift, int placesToShift) {
	int result = toShift;
	for (int i = 0; i < placesToShift; i++)
		result *= 3;
	return result;
}

int TernaryOperations::bitShiftRight(int toShift, int placesToShift) {
	int result = toShift;
	for (int i = 0; i < placesToShift; i++)
		result /= 3;
	return result;
}

int TernaryOperations::valueAtIndex(int number, int index) {
	int newNumber = bitShiftRight(number, index);
	int value = newNumber % 3;
	return value;
}

int TernaryOperations::removeValueAtIndex(int number, int index, int value) {
	int removeValue = bitShiftLeft(1, index) * value;
	return number - removeValue;
}
#pragma once

#include "Bag.h"
#include "stdafx.h"

Bag::Bag() {
	this->data = std::vector<int>();
	this->leftChild = NULL;
	this->rightChild = NULL;
}

Bag::Bag(std::vector<int> data, std::shared_ptr<Bag> leftChild, std::shared_ptr<Bag> rightChild) {
	this->data = data;
	this->leftChild = leftChild;	
	this->rightChild = rightChild;
}

std::vector<int> Bag::getData(){
	return data;
}

std::shared_ptr<Bag> Bag::getLeftChild(){
	return this->leftChild;
}

std::shared_ptr<Bag> Bag::getRightChild() {
	return this->rightChild;
}
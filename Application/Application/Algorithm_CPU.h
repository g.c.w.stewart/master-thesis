#pragma once

#include <vector>

#include "Graph.h"
#include "Instruction.h"
#include "Global.h"
#include "Algorithm.h"

class Algorithm_CPU : public Algorithm {
public:
	std::pair<int, double> runAlgorithm(TreeDecomposition* td);

	std::vector<std::vector<int>> indexLocations;
	std::vector<std::vector<int>> joinDataPerWidth;

protected:
	std::vector<std::vector<int>> oldValues;
	std::vector<int> newValues;

	//Functions
	virtual int sizeOfStepOrArray(int exponent) = 0;
	virtual int leafBag() = 0;
	virtual int introduceBag(int index, int node, bool isLast = true) = 0;
	virtual int introduceEdgeBag(int index, int node1, int node2) = 0;
	virtual int forgetBag(int index, int node1, int node2) = 0;
	virtual int joinBag(int index, int bagSize = -1) = 0;

};
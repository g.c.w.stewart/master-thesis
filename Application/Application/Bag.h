#pragma once

#include <vector>
#include <memory>

class Bag {
public:
	Bag();
	Bag(std::vector<int> data, std::shared_ptr<Bag> leftChild = NULL, std::shared_ptr<Bag> rightChild = NULL);
	std::vector<int> getData();
	std::shared_ptr<Bag>  getLeftChild();
	std::shared_ptr<Bag>  getRightChild();
private:
	std::vector<int> data;
	std::shared_ptr<Bag> leftChild;
	std::shared_ptr<Bag> rightChild;
};
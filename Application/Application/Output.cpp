#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>

#include "Output.h"
#include "Global.h"

bool fempty(const std::string& filename) {
	std::ifstream ifile(filename.c_str());
	return ifile.peek() == std::ifstream::traits_type::eof();
}

void Output::printToFile(std::string route,std::vector<std::string> results, int bagCount,
	int nodeCount, int edgeCount, int width,  int graphNumber) {
	std::string fileName = "";
	if (Global::useGPU) {
		if (Global::useMIS)
			fileName = "GPU_MIS.csv";
		else
			fileName = "GPU_DS.csv";
	}
	else {
		if (Global::useMIS)
			fileName = "CPU_MIS.csv";
		else
			fileName = "CPU_DS.csv";
	}

	std::ofstream myFile(route + fileName, std::ofstream::app);

	if (fempty	(route + fileName))
		myFile << "graph ; " << "GPU ; " << "MIS ; " << "width ; " << "# runs ; " << "# nodes ; " << "# edges ; " << "# bags ; " << "results ; " << std::endl;

	for (std::string result : results) {
		std::replace(result.begin(), result.end(), '.', ',');
		myFile << std::to_string(graphNumber) << " ; ";
		myFile << ((Global::useGPU) ? "GPU" : "CPU") << " ; ";
		myFile << ((Global::useMIS) ? "MIS" : "DS") << " ; ";
		myFile << width << " ; ";
		myFile << Global::numberOfRuns << " ; ";
		myFile << nodeCount << " ; ";
		myFile << edgeCount << " ; ";
		myFile << bagCount << " ; ";
		myFile << result << " ; ";
		myFile << std::endl;
	}

	myFile.close();
	}
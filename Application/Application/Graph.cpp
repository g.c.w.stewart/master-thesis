#pragma once

#include <vector>

#include "stdafx.h"
#include "Graph.h"
#include "Global.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>


TreeDecomposition::TreeDecomposition() {}

TreeDecomposition::TreeDecomposition(int nodeCount, int width) {
	this->width = width;
	this->numberOfNodes = nodeCount;
	this->Nodes = std::vector<TDNode*>(nodeCount);
}

TreeDecomposition::~TreeDecomposition() {
	for (TDNode* n : Nodes)
		delete n;

	Nodes.clear(); 
	Leaves.clear();
}

Vertex::Vertex() {}

Vertex::~Vertex() {
	Adjacent.clear();
}

Vertex::Vertex(int id) {
	this->id = id;
}

Edge::Edge(Vertex* from, Vertex* to) {
	this->From = from;
	this->To = to;
}

OG::OG() {

}

OG::~OG() {
	for (Vertex* v : Vertices)
		delete v;
	for (Edge* e : Edges)
		delete e;

	Vertices.clear();
	Edges.clear();
}

OG* OG::Parse(std::ifstream& inFile) {
	OG* G = new OG();
	int linenum = 1;
	for (std::string line; std::getline(inFile, line);) {
		linenum++;
		if (line == "END")
			break;
		std::vector < std::string > cf;
		std::istringstream iss(line);
		for (std::string line; iss >> line;)
			cf.push_back(line);
		if (cf[0] == "Nodes") {
			G->Vertices = std::vector<Vertex*>(std::stoi(cf[1]));
			for (int i = 0; i < std::stoi(cf[1]); i++)
				G->Vertices[i] = new Vertex(i);
		}
		if (cf[0] == "E")
			G->addEdge(std::stoi(cf[1]) - 1, std::stoi(cf[2]) - 1, G);
	}

	for (std::string line; std::getline(inFile, line);) {
		if (line == "END")
			break;
	}

	return G;
}

void OG::addEdge(int a, int b, OG* G) {

	Edge* newEdgeAB = new Edge(G->Vertices[a], G->Vertices[b]);
	Edge* newEdgeBA = new Edge(G->Vertices[b], G->Vertices[a]);
	G->Vertices[a]->Adjacent.push_back(newEdgeAB);
	G->Vertices[b]->Adjacent.push_back(newEdgeBA);

	if (a < b)
		G->Edges.push_back(newEdgeAB);
	else
		G->Edges.push_back(newEdgeBA);
}

 TreeDecomposition* TreeDecomposition::Parse(std::ifstream& inFile, OG* g) {
	TreeDecomposition* td = NULL;

	for (std::string line; std::getline(inFile, line);) {
		if (line == "END") {
			break;
		}
		std::vector < std::string > cf;
		std::istringstream iss(line);
		for (std::string line; iss >> line;)
			cf.push_back(line);
		if (cf.size() == 0) {
			continue;
		}

		if (cf[0] == "s") {
			td = new TreeDecomposition(std::stoi(cf[2]), std::stoi(cf[3]));
			int nodeSize = std::stoi(cf[2]);
		}
		else if (cf[0] == "b") {
			TDNode* newNode = new TDNode();
			newNode->ParentDecomposition = td;
			newNode->VertexByColor = std::vector<Vertex*>(td->width);
			for (unsigned int i = 2; i < cf.size(); i++)
				newNode->Bag.push_back(g->Vertices[std::stoi(cf[i]) - 1]);
			td->Nodes[std::stoi(cf[1]) - 1] = newNode;
		}
		else {
			try {
				int a = std::stoi(cf[0]);
				int b = std::stoi(cf[1]);
				td->Nodes[a - 1]->Adjacent.push_back(td->Nodes[b - 1]);
				td->Nodes[b - 1]->Adjacent.push_back(td->Nodes[a - 1]);
			}
			catch (...){}
		}
	}

	for (TDNode* n : td->Nodes) {
		if (n->Adjacent.size() == 1)
			td->Leaves.push_back(n);
	}

	td->ParentGraph = g;

	td->Nodes[0]->ColorVertices();

	return td;
}

void TreeDecomposition::FindOptimalRoot() {
	std::vector<TDNode*> NodesCopy = Nodes;
	int t = 0;

	while (t < 10000 && NodesCopy.size() > 0) {
		int p = rand() % NodesCopy.size();
		TDNode* n = NodesCopy[p];

		NodesCopy.erase(NodesCopy.begin() + p);

		double cost = n->CalculateCost();

		if (cost < EstimatedCost) {
			EstimatedCost = cost;
			Root = n;
		}
		t += Nodes.size();
	}

	Root->CalculateCost();

	for (TDNode* n : Nodes)
		n->SetBagsFinal();

	Root->PreJoinForget(NULL);

	Root->DeconstructJoins(NULL);

	Root->ForgetBeforeIntroduce(NULL);

	Root->IndexVertices(NULL);

	Root->FillSubtree(NULL);
}

std::vector<Instruction*> TreeDecomposition::Compute() {
	std::vector<Instruction*> traversal;
	Root->Compute(&traversal, NULL);
	Instruction* empty = new Instruction(Instruction::Empty);
	Root->ComputeForget(Root, NULL, Root->Bag, &traversal);
	traversal.push_back(empty);
	return traversal;
}
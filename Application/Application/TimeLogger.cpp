#include "stdafx.h"
#include "TimeLogger.h"

TimeLogger* TimeLogger::_instance = 0;

TimeLogger* TimeLogger::Instance() {
	if (_instance == 0)
		_instance = new TimeLogger;
	return _instance;
}

void TimeLogger::addTimeAndAnswer(int k, double time, int answer) {
	timeList[k] = time;
	answerList[k] = answer;
}

std::string TimeLogger::getTimesListString() {
	std::string result = "";
	for (int i = 1; i <= Global::numberOfRuns; i++)
		result += std::to_string(i) + "," + std::to_string(answerList[i]) + "," + std::to_string(timeList[i]) + "\n";
	flushLists();
	return result;
}

void TimeLogger::flushLists() {
	for (int i = 0; i <= Global::numberOfRuns; i++) {
		timeList[i] = -1;
		answerList[i] = -1;
	}
}





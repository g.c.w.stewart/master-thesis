#pragma once
#include <string>
#include <vector>

class Output {
public:
	static void printToFile(std::string route, std::vector<std::string> results,
		int bagCount, int nodeCount, int edgeCount, int width, int graphNumber);
};
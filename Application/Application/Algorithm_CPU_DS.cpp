#include "stdafx.h"
#include "Algorithm_CPU_DS.h"
#include <algorithm>  
#include "TernaryOperations.h"

Algorithm_CPU_DS::Algorithm_CPU_DS(std::vector<Instruction*> traversal) {
	this->traversal = traversal;
}

int Algorithm_CPU_DS::sizeOfStepOrArray(int exponent) {
	return TernaryOperations::bitShiftLeft(1, exponent);
}

int Algorithm_CPU_DS::leafBag() {
	return 0;
}

int Algorithm_CPU_DS::introduceBag(int index, int nodeIndex, bool isLast) {
	int colour = TernaryOperations::valueAtIndex(index, nodeIndex);
	int nodePositionMask = 1 << nodeIndex;

	if (colour == 0)
		return Global::maxValue;

	if (isLast) {
		int newIndex = TernaryOperations::removeValueAtIndex(index, nodeIndex, colour);
		return oldValues.back()[newIndex];
	}
	else {
		int leftSide = TernaryOperations::bitShiftRight(index, nodeIndex);
		leftSide = TernaryOperations::bitShiftLeft(leftSide, nodeIndex);
		int rightSide = index - leftSide;
		leftSide = TernaryOperations::bitShiftRight(leftSide, nodeIndex + 1);
		leftSide = TernaryOperations::bitShiftLeft(leftSide, nodeIndex);

		return oldValues.back()[leftSide + rightSide];
	}
}

int Algorithm_CPU_DS::introduceEdgeBag(int index, int node1Index, int node2Index) {
	int node1Value = TernaryOperations::valueAtIndex(index, node1Index);
	int node2Value = TernaryOperations::valueAtIndex(index, node2Index);

	if ((node1Value == 2 && node2Value == 0) || (node1Value == 0 && node2Value == 2)) {
		if (node1Value == 0) {
			int positionMask1 = TernaryOperations::bitShiftLeft(1, node1Index);
			return oldValues.back()[index + positionMask1];
		}
		else {
			int positionMask2 = TernaryOperations::bitShiftLeft(1, node2Index);
			return oldValues.back()[index + positionMask2];
		}
	}
	else
		return oldValues.back()[index];
}

int Algorithm_CPU_DS::forgetBag(int index, int previousNodeIndex, int thisStepSize) {
	int positionMask = TernaryOperations::bitShiftLeft(1, previousNodeIndex);

	int leftSide = TernaryOperations::bitShiftLeft(TernaryOperations::bitShiftRight(index, previousNodeIndex), previousNodeIndex);
	int rightSide = index - leftSide;
	int combined = TernaryOperations::bitShiftLeft(leftSide, 1) + rightSide;

	int option1Index = combined;
	int option2index = combined + positionMask * 2;

	int option1 = oldValues.back()[option1Index];
	int option2 = oldValues.back()[option2index] + 1;

	if (option1 > option2)
		return option2;
	else
		return option1;
}

//int Algorithm_CPU_DS::joinBag(int index, int bagSize)	 {
//	int answer = Global::maxValue;
//
//	int joinIndex = this->indexLocations[bagSize][index];
//	int zeroCount = this->joinDataPerWidth[bagSize][joinIndex];
//	std::vector<int> child1 = this->oldValues.back();
//	std::vector<int> child2 = this->oldValues.end()[-2];
//
//	for (int j = 0; j < (1 << zeroCount); j++) {
//		int indexLeft = joinIndex + 2 + zeroCount + (j*2);
//		int indexRight = joinIndex + 2 + zeroCount + (j*2) + 1;
//		int left = this->joinDataPerWidth[bagSize][indexLeft];
//		int right = this->joinDataPerWidth[bagSize][indexRight];
//
//		int old = child1[left] + child2[right];
//
//		if (answer > old) {
//			answer = old;
//		}
//	}
//
//	return answer;
//}


int Algorithm_CPU_DS::joinBag(int index, int bagSize)	 {
	// initialize variables
	int answer = Global::maxValue;
	int k = 0;
	std::vector<int> zeroPositions;
	int leftValue = index;
	int rightValue = index;
	std::vector<int> child1 = this->oldValues.back();
	std::vector<int> child2 = this->oldValues.end()[-2];



	// Compute bitmask a, bitmask b, k and save the zero positions
	for (int i = 0; i < bagSize; i++) {
		int value = TernaryOperations::valueAtIndex(index, i); // get the value at position i in the index
		if (value == 0) {
			zeroPositions.push_back(i);
			rightValue += TernaryOperations::bitShiftLeft(1, i);
			k++;
		}
	}

	// loop over the amount of locations for 0
	for (int Z = 0; Z < pow(2, k); Z++) {
		int copyRight = rightValue;
		int copyLeft = leftValue;

		// loop over the bits in Z that might be 0
		for (int i = 0; i < bagSize; i++) {
			int value = Z >> i;
			int bitmask = 1;
			value = value & bitmask;

			if (value == 1) {
				copyLeft += TernaryOperations::bitShiftLeft(1, zeroPositions[i]);
				copyRight -= TernaryOperations::bitShiftLeft(1, zeroPositions[i]);
			}
		}

		int result = child1[copyLeft] + child2[copyRight];
		if ( result < answer)
			answer = child1[copyLeft] + child2[copyRight];
	}


	return answer;
}
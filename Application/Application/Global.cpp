#pragma once
#include "stdafx.h"

#include "Global.h"
#include "algorithm"

namespace Global {
	extern const bool useGPU = true;
	extern const bool useMIS = false;
	extern const bool useIntroduceEdge = true;	
	extern const bool multipleRuns = false;
	extern const bool staticBagSize = false;
	extern const bool dealocMem = false;
	extern const int numberOfRuns = 5;
	extern const int numberOfGraphs = 200; // max: 200
	extern const unsigned int indicesPerThread = 1;

	extern const int minValue = std::numeric_limits<int>::min() / 2;
	extern const int maxValue = std::numeric_limits<int>::max() / 2;
}
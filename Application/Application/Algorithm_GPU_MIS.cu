#include <iostream>
#include <vector>
#include <algorithm>

#include "Algorithm_GPU_MIS.cuh"

Algorithm_GPU_MIS::Algorithm_GPU_MIS(std::vector<Instruction*> traversal) {
	this->traversal = traversal;
}

int Algorithm_GPU_MIS::sizeOfStepOrArray(int exponent) {
	return 1 << exponent;
}

__global__ void leafBagKernelMIS(int *newValues) {
	// sets the index of the empty set to 0
	int index = (blockIdx.x * blockDim.x) + threadIdx.x;
	newValues[index] = (short)0;
}

__global__ void introduceBagKernelMIS(int *newValues, int *nodeIndex, int *oldValues, int indicesPerThread, bool isLast = true) {
	for (int i = 0; i < indicesPerThread; i++) {
		int index = (blockIdx.x * blockDim.x) + (threadIdx.x * indicesPerThread) + i;
		int positionMask = 1 << nodeIndex[1];
		int isInSet = (index & positionMask) >> nodeIndex[1];
		
		if (isLast) {
			if (isInSet == 1) {
				int newIndex = index - positionMask;
				newValues[index] = oldValues[newIndex];
			}
			else {
				newValues[index] = oldValues[index];
			}
		}
		else {
			if (isInSet == 1) {
				int leftside = index >> nodeIndex[1];
				leftside = leftside << nodeIndex[1];
				int rightside = index - leftside;
				leftside = leftside >> (nodeIndex[1] + 1);
				leftside = leftside << nodeIndex[1];
				newValues[index] = oldValues[leftside + rightside];
			}
			else {
				int leftside = index >> (nodeIndex[1] + 1);
				leftside = leftside << nodeIndex[1];
				int rightside = index - (leftside << 1);
				newValues[index] = oldValues[leftside + rightside];
			}
		}
	}

}

__global__ void introduceEdgeBagKernelMIS(int *newValues, int *nodeIndex, int *oldValues, int indicesPerThread) {
	for (int i = 0; i < indicesPerThread; i++) {
		int index = (blockIdx.x * blockDim.x) + (threadIdx.x * indicesPerThread) + i;

		//Masks for checking whether the set contains the first node
		int positionMask1 = 1 << nodeIndex[1];
		int isInSet1 = (index & positionMask1) >> nodeIndex[1];

		//Masks for checking whether the set contains the second node
		int positionMask2 = 1 << nodeIndex[3];
		int isInSet2 = (index & positionMask2) >> nodeIndex[3];

		//Return sentinel if both nodes are in the set
		if (isInSet1 == 1) {
			if (isInSet2 == 1) {
				newValues[index] = (short)-32000;
			}
			else {
				newValues[index] = oldValues[index];
			}
		}
		else {
			newValues[index] = oldValues[index];
		}
	}
}

__global__ void forgetBagKernelMIS(int *newValues, int *nodeIndex, int *oldValues, int calledValue, int indicesPerThread) {
	for (int i = 0; i < indicesPerThread; i++) {
		int index = (blockIdx.x * blockDim.x) + (threadIdx.x * indicesPerThread) + i;
		int oldStepSize = calledValue << 1;

		int leftMask = (oldStepSize - 1) & ((oldStepSize - 1) << nodeIndex[1]);
		int rightMask = (oldStepSize - 1) - leftMask;

		int leftSide = (index & leftMask) << 1;
		int rightSide = (index & rightMask);
		int combined = leftSide + rightSide;

		int indexOption1 = combined; //Set without node
		int indexOption2 = combined + (1 << nodeIndex[1]); //Set with node

		short option1 = oldValues[indexOption1];
		short option2 = oldValues[indexOption2] + 1;

		if (option1 > option2)
			newValues[index] = option1;
		else
			newValues[index] = option2;
	}
}

__global__ void joinBagKernelMIS(int *newValues, int *nodeIndex, int *oldValues1, int *oldValues2, int indicesPerThread) {
	for (int i = 0; i < indicesPerThread; i++) {
		int index = (blockIdx.x * blockDim.x) + (threadIdx.x * indicesPerThread) + i;

		int valuePreviousBag = oldValues1[index];
		int valuePreviousBag2 = oldValues2[index];

		if (valuePreviousBag < 0 || valuePreviousBag2 < 0)
			newValues[index] = (short)-32000;
		else
			newValues[index] = valuePreviousBag + valuePreviousBag2;
	}
}

cudaError_t Algorithm_GPU_MIS::launchKernel(unsigned int newSize, Instruction::InstructionType instructionType, int bagSize, bool isLast) {
	cudaError_t cudaStatus;

	//std::pair<int, int> kernelParams;
	//int blocks;
	//int threads;

	//if (instructionType != Instruction::Introduce) {
	//	kernelParams = getKernelParameters(newSize);
	//	blocks = kernelParams.first;
	//	threads = kernelParams.second;
	//}
	//else {
	//	kernelParams = getKernelParameters(newSize >> 1);
	//	blocks = kernelParams.first;
	//	threads = kernelParams.second;
	//}

	std::pair<int, int> kernelParams = getKernelParameters(newSize);
	int blocks = kernelParams.first;
	int threads = kernelParams.second;

	

	int* dev_newValues = valuePointers.back();
	int* dev_oldValues;
	if (instructionType != Instruction::Leaf)
		dev_oldValues = valuePointers.end()[-2];

	//size_t free_byte;
	//size_t total_byte;
	//cudaStatus = cudaMemGetInfo(&free_byte, &total_byte);


	if (instructionType == Instruction::Leaf) {
		leafBagKernelMIS<<<blocks, threads>>>(dev_newValues);
	}
	else if (instructionType == Instruction::Introduce) {
		introduceBagKernelMIS<<<blocks, threads>>>(dev_newValues, dev_nodeIndex, dev_oldValues, Global::indicesPerThread, isLast);
	}
	else if (instructionType == Instruction::IntroduceEdge) {
		introduceEdgeBagKernelMIS<<<blocks, threads>>>(dev_newValues, dev_nodeIndex, dev_oldValues, Global::indicesPerThread);
	}
	else if (instructionType == Instruction::Forget) {
		forgetBagKernelMIS<<<blocks, threads>>>(dev_newValues, dev_nodeIndex, dev_oldValues, blocks * threads * Global::indicesPerThread, Global::indicesPerThread);
	}
	else if (instructionType == Instruction::Join) {
		joinBagKernelMIS <<<blocks, threads >>>(dev_newValues, dev_nodeIndex, dev_oldValues, valuePointers.end()[-3], Global::indicesPerThread);
	}
	cudaStatus = cudaGetLastError();
	return cudaStatus;
}

std::pair<int, int> Algorithm_GPU_MIS::getKernelParameters(unsigned int newSize) {
	int blocks;
	int threads;

	if (newSize <= Global::indicesPerThread) {
		blocks = 1;
		threads = 1;
	}
	else {
		int numberofthreadsNeeded = newSize / Global::indicesPerThread;
		int index = (int)log2(numberofthreadsNeeded);

		if (index <= 10) {
			blocks = 1;
			threads = numberofthreadsNeeded;
		}
		else {
			blocks = 1 << (index - 10);
			threads = 1024;
		}
	}
	return std::pair<int, int>(blocks, threads);
}
